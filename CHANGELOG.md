# Current
* source encoding
* enforcer- and versions-plugin for Maven maintenance
* pluginManagement

# Rejected

## Split Source- and Target-level

While it is absolutely feasible to use a more recent language version and down-compile to a previous version it introduces security issues. For example when writing this JDK 8 was End-Of-Life and in broad use – for no other obvious reason but lack of money. I don't think it is a good idea to still provide tools to support such a dangerous situation.
