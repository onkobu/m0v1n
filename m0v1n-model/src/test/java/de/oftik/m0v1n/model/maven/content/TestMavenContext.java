package de.oftik.m0v1n.model.maven.content;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.VersionProperties;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;

class TestMavenContext implements MavenContext {
	@Override
	public void setFlag(final Flag flag) {
		unexpected();
	}

	private <T> T unexpected() {
		throw new AssertionError("unexpected invocation");
	}

	@Override
	public boolean isSet(final Flag f) {
		return unexpected();
	}

	@Override
	public void forcePlugin(final String artifactId, final Version version, final Comment comment) {
		unexpected();
	}

	@Override
	public void forcePlugin(final Plugin p) {
		unexpected();
	}

	@Override
	public InputValue findValue(final String string) {
		return unexpected();
	}

	@Override
	public String getVersionProperty(final String key, final String defaultValue) {
		return unexpected();
	}

	@Override
	public VersionProperties getVersionProperties() {
		return unexpected();
	}

	@Override
	public String nextLine() {
		return unexpected();
	}

	@Override
	public InputValue projectGroupId() {
		return unexpected();
	}

	@Override
	public InputValue projectArtifactId() {
		return unexpected();
	}
}
