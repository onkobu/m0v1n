package de.oftik.m0v1n.model.maven.content;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import de.oftik.m0v1n.model.content.SkippableSection;
import de.oftik.m0v1n.model.format.TabIndent;
import de.oftik.m0v1n.model.maven.VersionProperties;

class OwaspDependencyCheckTest {
	@Test
	void noVersion() {
		assertThat(new OwaspDependencyCheck().getVersion(), Matchers.nullValue());
	}

	@Test
	void noDefaultVersion() {
		final var p = new Properties();
		final var props = new VersionProperties(p);
		assertThat(new OwaspDependencyCheck().getVersion(props),
				Matchers.equalTo(Constants.OWASP_DEPENDENCY_CHECK_VERSION));
	}

	@Test
	void defaultVersion() {
		final var odc = new OwaspDependencyCheck();
		final var p = new Properties();
		// something unusual for a standard plugin
		final var v = "1.0.0-SNAPSHOT";
		p.put(odc.getFullName(), v);
		final var props = new VersionProperties(p);
		assertThat(odc.getVersion(props), Matchers.equalTo(Version.plainVersion(v)));
	}

	@Test
	void wrapInSkippableWithNo() throws Exception {
		final var skipSect = new SkippableSection("lalala", new OwaspDependencyCheck());
		final var ctx = new TestMavenContext() {
			private final Iterator<String> lines = Arrays.asList("n").iterator();
			private final VersionProperties props = new VersionProperties(new Properties());

			@Override
			public VersionProperties getVersionProperties() {
				return props;
			}

			@Override
			public String nextLine() {
				return lines.next();
			}
		};
		skipSect.requestInput(ctx);
		final var sw = new StringWriter();
		skipSect.writeTo(sw, new TabIndent(), ctx);
		assertThat(sw.toString(), Matchers.equalTo(""));
	}

	@Test
	void wrapInSkippableWithYes() throws Exception {
		final var skipSect = new SkippableSection("lalala", new OwaspDependencyCheck());
		final var ctx = new TestMavenContext() {
			private final Iterator<String> lines = Arrays.asList("y", "1.0.0-SNAPSHOT").iterator();
			private final VersionProperties props = new VersionProperties(new Properties());

			@Override
			public VersionProperties getVersionProperties() {
				return props;
			}

			@Override
			public String nextLine() {
				return lines.next();
			}
		};
		skipSect.requestInput(ctx);
		final var sw = new StringWriter();
		skipSect.writeTo(sw, new TabIndent(), ctx);
		assertThat(sw.toString(), Matchers.equalTo("<plugin>\n"//
				+ "\t<groupId>org.owasp</groupId>\n"//
				+ "\t<artifactId>dependency-check-maven</artifactId>\n"//
				+ "\t<version>1.0.0-SNAPSHOT</version>\n"//
				+ "\t<executions>\n"//
				+ "\t\t<execution>\n"//
				+ "\t\t\t<goals>\n"//
				+ "\t\t\t\t<goal>check</goal>\n"//
				+ "\t\t\t</goals>\n"//
				+ "\t\t</execution>\n"//
				+ "\t</executions>"//
				+ "\n</plugin>\n"));
	}
}
