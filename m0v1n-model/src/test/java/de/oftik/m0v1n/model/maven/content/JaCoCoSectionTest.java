package de.oftik.m0v1n.model.maven.content;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import de.oftik.m0v1n.model.input.InputValue;

class JaCoCoSectionTest {
	@Test
	void inputForLimit() {
		final var sec = new JaCoCoSection();
		final var values = new ArrayList<InputValue>();
		final var inputValue = "2.80";
		final var parsedValue = inputValue.replace('.', ',');
		final var ctx = new TestMavenContext() {
			private final Iterator<String> lines = Arrays.asList(inputValue, inputValue, inputValue, inputValue)
					.iterator();

			@Override
			public String nextLine() {
				return lines.next();
			}
		};
		for (final var attr : sec.getAttributes()) {
			attr.requestInput(ctx);
			values.add(attr.getInputValue());
		}
		MatcherAssert.assertThat(values.stream().map(InputValue::get).collect(Collectors.joining(", ")),
				Matchers.equalTo(parsedValue + ", " + parsedValue + ", " + parsedValue + ", " + parsedValue));
	}
}
