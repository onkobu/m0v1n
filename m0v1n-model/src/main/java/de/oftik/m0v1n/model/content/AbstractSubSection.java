package de.oftik.m0v1n.model.content;

import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.ComplexSection;
import de.oftik.m0v1n.model.contract.SubSection;
import de.oftik.m0v1n.model.maven.particles.Comment;

/**
 * A smaller group with a section.
 *
 * @author onkobu
 *
 */
public abstract class AbstractSubSection extends AbstractSection implements SubSection {

	private ComplexSection parent;

	private final Comment comment;

	protected AbstractSubSection(final String name, final Attribute... attributes) {
		this(name, null, attributes);
	}

	protected AbstractSubSection(final String name, final Comment comment, final Attribute... attributes) {
		super(name, attributes);
		this.comment = comment;
	}

	@Override
	public void setParent(final ComplexSection parent) {
		this.parent = parent;
	}

	protected ComplexSection getComplexParent() {
		return parent;
	}

	@Override
	public Comment getComment() {
		return comment;
	}
}
