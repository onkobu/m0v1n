package de.oftik.m0v1n.model.maven.particles;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.VersionProperties;
import de.oftik.m0v1n.model.maven.content.Dependency;
import de.oftik.m0v1n.model.maven.content.GroupTagName;
import de.oftik.m0v1n.model.maven.content.InlineTagName;
import de.oftik.m0v1n.model.maven.content.Version;

/**
 * A general plugin element.
 *
 * @author onkobu
 *
 */
public class Plugin {
	private final String groupId;
	private final String artifactId;
	private Version version;
	private final Comment comment;
	private Collection<Execution> executions = new ArrayList<>();
	private Collection<Dependency> dependencies = new ArrayList<>();

	private final EnumSet<SubSection> subSections;
	private Collection<Configuration> configurations = new ArrayList<>();

	public enum SubSection {
		configuration(GroupTagName.configuration),

		executions(GroupTagName.executions),

		dependencies(GroupTagName.dependencies);

		private final GroupTagName tagName;

		SubSection(final GroupTagName tagName) {
			this.tagName = tagName;
		}
	}

	public abstract static class Execution {
		private final String comment;

		public Execution() {
			this(null);
		}

		public Execution(final String comment) {
			this.comment = comment;
		}

		public final void writeTo(final Writer writer, final Indent indent) throws IOException {
			if (comment != null) {
				PomXml.comment(writer, comment, indent);
			}
			PomXml.openTag(writer, GroupTagName.execution, indent);
			writeContent(writer, indent);
			PomXml.closeTag(writer, GroupTagName.execution, indent);
		}

		protected abstract void writeContent(Writer writer, Indent indent) throws IOException;
	}

	public enum Goal {
		compile, check;
	}

	public static final class ExecutionForGoal extends Execution {
		private final Goal goal;

		public ExecutionForGoal(final Goal g) {
			this.goal = g;
		}

		@Override
		protected void writeContent(final Writer writer, final Indent indent) throws IOException {
			PomXml.openTag(writer, GroupTagName.goals, indent);
			PomXml.writeValue(writer, InlineTagName.goal, goal.name(), indent);
			PomXml.closeTag(writer, GroupTagName.goals, indent);
		}
	}

	public abstract static class Configuration {
		private final String comment;

		public Configuration() {
			this(null);
		}

		public Configuration(final String comment) {
			this.comment = comment;
		}

		public final void writeTo(final Writer writer, final Indent indent) throws IOException {
			if (comment != null) {
				PomXml.comment(writer, comment, indent);
			}
			writeContent(writer, indent);
		}

		protected abstract void writeContent(Writer writer, Indent indent) throws IOException;
	}

	public Plugin(final String artifactId, final Version version, final Comment comment) {
		this(null, artifactId, version, comment, EnumSet.noneOf(SubSection.class));
	}

	public Plugin(final String groupId, final String artifactId, final Version version, final Comment comment,
			final EnumSet<SubSection> subSections) {
		super();
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.version = version;
		this.comment = comment;
		this.subSections = subSections;
	}

	public Plugin addExecution(final Execution execution) {
		executions.add(execution);
		return this;
	}

	public Plugin addDependency(final Dependency dep) {
		dependencies.add(dep);
		return this;
	}

	public Plugin addConfiguration(final Configuration config) {
		configurations.add(config);
		return this;
	}

	/**
	 *
	 * @return Only <code>null</code> for Maven plugins.
	 */
	public String getGroupId() {
		return groupId;
	}

	public String getArtifactId() {
		return artifactId;
	}

	/**
	 * Tries various sources if no default was given.
	 *
	 * @param props
	 * @return
	 */
	public Version getVersion(final VersionProperties props) {
		if (props.containsKey(getFullName())) {
			return Version.plainVersion(props.get(getFullName()));
		}
		if (version.isEmpty() || version != null) {
			return version;
		}
		return Version.DEFAULT_VERSION;
	}

	/**
	 * Only the value set without further attempts.
	 *
	 * @return
	 */
	public Version getVersion() {
		return version;
	}

	public void setVersion(final Version v) {
		version = v;
	}

	public Comment getComment() {
		return comment;
	}

	public Plugin merge(final Plugin other) {
		if (!other.executions.isEmpty()) {
			executions.addAll(other.executions);
		}

		if (!other.dependencies.isEmpty()) {
			dependencies.addAll(other.dependencies);
		}
		return this;
	}

	public final void writeTo(final Writer w, final Indent indent, MavenContext ctx) throws IOException {
		if (comment != null) {
			indent.indent(w);
			comment.writeTo(w);
		}
		PomXml.openTag(w, GroupTagName.plugin, indent);
		if (groupId != null) {
			PomXml.writeValue(w, InlineTagName.groupId, groupId, indent);
		}
		PomXml.writeValue(w, InlineTagName.artifactId, artifactId, indent);
		if (version != null) {
			PomXml.writeValue(w, InlineTagName.version, version.toString(), indent);
		}
		if (subSections.contains(SubSection.configuration) && !configurations.isEmpty()) {
			PomXml.openTag(w, SubSection.configuration.tagName, indent);
			writeConfiguration(w, indent);
			PomXml.closeTag(w, SubSection.configuration.tagName, indent);
		}

		if (subSections.contains(SubSection.executions) && !executions.isEmpty()) {
			PomXml.openTag(w, SubSection.executions.tagName, indent);
			writeExecutions(w, indent);
			PomXml.closeTag(w, SubSection.executions.tagName, indent);
		}
		if (subSections.contains(SubSection.dependencies) && !dependencies.isEmpty()) {
			PomXml.openTag(w, SubSection.dependencies.tagName, indent);
			writeDependencies(w, indent, ctx);
			PomXml.closeTag(w, SubSection.dependencies.tagName, indent);
		}
		PomXml.closeTag(w, GroupTagName.plugin, indent);
	}

	public boolean samePlugin(final Plugin other) {
		return groupId.equals(other.getGroupId()) && artifactId.equals(other.getArtifactId());
	}

	protected void writeConfiguration(final Writer w, final Indent indent) throws IOException {
		for (final Configuration c : configurations) {
			c.writeTo(w, indent);
		}
	}

	protected void writeExecutions(final Writer w, final Indent indent) throws IOException {
		// Streams don't work well with exceptions
		for (final Execution e : executions) {
			e.writeTo(w, indent);
		}
	}

	protected void writeDependencies(final Writer w, final Indent indent, MavenContext ctx) throws IOException {
		// Streams don't work well with exceptions
		for (final Dependency d : dependencies) {
			d.writeTo(w, indent, ctx);
		}
	}

	public String getFullName() {
		return String.format("%s:%s", groupId, artifactId);
	}
}
