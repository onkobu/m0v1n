package de.oftik.m0v1n.model.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;

/**
 * An attribute that requires a yes/no decision.
 *
 * @author onkobu
 *
 */
public final class YesNoAttribute extends AbstractAttribute {
	private boolean yes;
	private final NamedWritable delegate;

	public YesNoAttribute(final NamedWritable delegate) {
		super(delegate.getName());
		this.delegate = delegate;
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		System.out.printf("Enable %s Y/(N): ", getName());
		final String value = ctx.nextLine();
		yes = "y".equalsIgnoreCase(value);
	}

	@Override
	public final void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
		if (isYes()) {
			registerPropertyIfYes(properties, ctx);
		}
	}

	protected void registerPropertyIfYes(Map<String, String> properties, MavenContext ctx) {
		if (isYes()) {
			delegate.registerProperty(properties, ctx);
		}
	}

	@Override
	public final void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		if (isYes()) {
			delegate.writeTo(writer, indent, ctx);
		}
	}

	public boolean isYes() {
		return yes;
	}

	@Override
	public InputValue getInputValue() {
		return isYes() ? InputValue.YES_VALUE : InputValue.NO_VALUE;
	}
}
