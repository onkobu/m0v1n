package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.EnumSet;

import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * Compile plugin.
 *
 * @author onkobu
 *
 */
public class CompilerPlugin extends Plugin {
	private boolean writeJavaVersion;

	public CompilerPlugin() {
		this(null);
	}

	public CompilerPlugin(final Version version) {
		super("org.apache.maven.plugins", "maven-compiler-plugin", version, null,
				EnumSet.of(SubSection.configuration, SubSection.executions));
	}

	public CompilerPlugin withJDKVersionReference() {
		writeJavaVersion = true;
		return this;
	}

	public CompilerPlugin withVersion(final Version v) {
		setVersion(v);
		return this;
	}

	@Override
	public Plugin merge(final Plugin other) {
		super.merge(other);
		final CompilerPlugin otherC = (CompilerPlugin) other;
		if (getVersion() == null && otherC.getVersion() != null) {
			setVersion(otherC.getVersion());
		} else if (getVersion() != null && otherC.getVersion() != null && !getVersion().equals(otherC.getVersion())) {
			throw new IllegalStateException(String.format("%s.%s version %s and %s cannot be merged", getGroupId(),
					getArtifactId(), getVersion(), otherC.getVersion()));
		}

		if (otherC.writeJavaVersion) {
			writeJavaVersion = true;
		}
		return this;
	}

	@Override
	protected void writeConfiguration(final Writer w, final Indent indent) throws IOException {
		if (!writeJavaVersion) {
			return;
		}
		PomXml.writeFreeValue(w, "source", Version.versionProperty("java.version").toString(), indent);
		PomXml.writeFreeValue(w, "target", Version.versionProperty("java.version").toString(), indent);
	}
}