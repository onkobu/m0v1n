package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.EnumSet;

import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;
import de.oftik.m0v1n.model.maven.particles.Plugin.Execution;

/**
 * Specific dependency.
 *
 * @author onkobu
 *
 */
public class DependencyKotlin extends Dependency {
	public DependencyKotlin() {
		super("org.jetbrains.kotlin", "kotlin-stdlib", "Dependency Kotlin", "version.kotlin",
				Version.plainVersion("1.3.72"), null, new KotlincPlugin(),
				new CompilerPlugin(Version.plainVersion("3.5.1"))
						.addExecution(new Execution("Replacing default-compile as it is treated specially by maven") {
							@Override
							protected void writeContent(final Writer writer, final Indent indent) throws IOException {
								PomXml.writeValue(writer, InlineTagName.id, "default-compile", indent);
								PomXml.writeValue(writer, InlineTagName.phase, "none", indent);
							}
						}).addExecution(
								new Execution("Replacing default-testCompile as it is treated specially by maven") {
									@Override
									protected void writeContent(final Writer writer, final Indent indent)
											throws IOException {
										PomXml.writeValue(writer, InlineTagName.id, "default-testCompile", indent);
										PomXml.writeValue(writer, InlineTagName.phase, "none", indent);
									}
								})
						.addExecution(new Execution() {
							@Override
							protected void writeContent(final Writer writer, final Indent indent) throws IOException {
								PomXml.writeValue(writer, InlineTagName.id, "java-compile", indent);
								PomXml.writeValue(writer, InlineTagName.phase, "compile", indent);
								PomXml.openTag(writer, GroupTagName.goals, indent);
								PomXml.writeValue(writer, InlineTagName.goal, "compile", indent);
								PomXml.closeTag(writer, GroupTagName.goals, indent);
							}
						}).addExecution(new Execution() {
							@Override
							protected void writeContent(final Writer writer, final Indent indent) throws IOException {
								PomXml.writeValue(writer, InlineTagName.id, "java-test-compile", indent);
								PomXml.writeValue(writer, InlineTagName.phase, "test-compile", indent);
								PomXml.openTag(writer, GroupTagName.goals, indent);
								PomXml.writeValue(writer, InlineTagName.goal, "testCompile", indent);
								PomXml.closeTag(writer, GroupTagName.goals, indent);
							}
						}));
	}

	/**
	 * Define from where kotlinc reads sources. Allows usage of Java classes of the
	 * same project.
	 *
	 * @author onkobu
	 *
	 */
	static class KotlincPlugin extends Plugin {
		public KotlincPlugin() {
			super("org.jetbrains.kotlin", "kotlin-maven-plugin", Version.versionProperty("version.kotlin"),
					new Comment("to put kotlinc before javac"), EnumSet.of(SubSection.executions));
			addExecution(new Execution() {
				@Override
				protected void writeContent(final Writer writer, final Indent indent) throws IOException {
					PomXml.writeValue(writer, InlineTagName.id, "compile", indent);
					PomXml.openTag(writer, GroupTagName.goals, indent);
					PomXml.writeValue(writer, InlineTagName.goal, "compile", indent);
					PomXml.closeTag(writer, GroupTagName.goals, indent);
					PomXml.openTag(writer, GroupTagName.configuration, indent);
					PomXml.openTag(writer, GroupTagName.sourceDirs, indent);
					PomXml.writeValue(writer, InlineTagName.sourceDir, "${project.basedir}/src/main/kotlin", indent);
					PomXml.writeValue(writer, InlineTagName.sourceDir, "${project.basedir}/src/main/java", indent);
					PomXml.closeTag(writer, GroupTagName.sourceDirs, indent);
					PomXml.closeTag(writer, GroupTagName.configuration, indent);
				}
			});
			addExecution(new Execution() {
				@Override
				protected void writeContent(final Writer writer, final Indent indent) throws IOException {
					PomXml.writeValue(writer, InlineTagName.id, "test-compile", indent);
					PomXml.openTag(writer, GroupTagName.goals, indent);
					PomXml.writeValue(writer, InlineTagName.goal, "test-compile", indent);
					PomXml.closeTag(writer, GroupTagName.goals, indent);
					PomXml.openTag(writer, GroupTagName.configuration, indent);
					PomXml.openTag(writer, GroupTagName.sourceDirs, indent);
					PomXml.writeValue(writer, InlineTagName.sourceDir, "${project.basedir}/src/test/kotlin", indent);
					PomXml.writeValue(writer, InlineTagName.sourceDir, "${project.basedir}/src/test/java", indent);
					PomXml.closeTag(writer, GroupTagName.sourceDirs, indent);
					PomXml.closeTag(writer, GroupTagName.configuration, indent);
				}
			});
		}
	}
}
