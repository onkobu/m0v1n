package de.oftik.m0v1n.model.maven.content;

/**
 * Constants.
 *
 * @author onkobu
 *
 */
public final class Constants {
	private Constants() {
		// not to be instantiated
	}

	public static final Version ENFORCE_MAVEN_VERSION = Version.plainVersion("3.6");

	public static final Version OWASP_DEPENDENCY_CHECK_VERSION = Version.plainVersion("6.5.1");
}
