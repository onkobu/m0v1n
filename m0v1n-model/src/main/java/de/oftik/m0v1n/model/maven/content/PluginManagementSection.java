package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.oftik.m0v1n.model.content.SkippableSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Plugin;

public class PluginManagementSection extends SkippableSection {
	private final List<Plugin> plugins = new ArrayList<>();

	public PluginManagementSection() {
		super("Plugin Management", Collections.emptyList());
	}

	@Override
	public void writeTo(Writer writer, Indent indent, MavenContext ctx) throws IOException {
		if (plugins.isEmpty()) {
			return;
		}
		PomXml.openTag(writer, GroupTagName.pluginManagement, indent);
		PomXml.openTag(writer, GroupTagName.plugins, indent);
		for (Plugin p : plugins) {
			p.writeTo(writer, indent, ctx);
		}
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.plugins, indent);
		PomXml.closeTag(writer, GroupTagName.pluginManagement, indent);
	}

	public void forcePlugin(Plugin plugin) {
		final Optional<Plugin> merged = plugins.stream().filter(p -> p.samePlugin(plugin)).findFirst()
				.map(found -> found.merge(plugin));
		if (merged.isEmpty()) {
			plugins.add(plugin);
		}
	}
}
