package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import de.oftik.kehys.lippu.Issue;
import de.oftik.kehys.lippu.Reason;
import de.oftik.kehys.lippu.Refactor;
import de.oftik.kehys.lippu.Severity;
import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.MavenContext.Flag;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;

/**
 * &lt;dependencyManagement&gt;.
 *
 * @author onkobu
 *
 */
public class DependencyManagementSection extends DependenciesSection {

	public DependencyManagementSection() {
		super();
	}

	@Refactor(@Issue(author = "onkobu", dateFound = "Nov.2021", reason = Reason.NOT_LISKOV_SUBSTITUE, severity = Severity.HIGH, value = "Any attribute could be put in, not only dependencies"))
	public DependencyManagementSection(final Attribute... attributes) {
		super(attributes);
	}

	public DependencyManagementSection(final Collection<Attribute> attributes) {
		super(attributes);
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
		ctx.setFlag(Flag.PACKAGING_POM);
		super.adjustContext(ctx);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.dependencyManagement, indent);
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.dependencyManagement, indent);
	}
}
