package de.oftik.m0v1n.model.maven.content;

import java.util.Objects;

public class Version {
	/**
	 * A version that is skipped in output.
	 */
	public static final Version EMPTY_VERSION = new Version(null);

	/*
	 * A version that replaces all null values.
	 */
	public static final Version DEFAULT_VERSION = new Version("1.0.0-SNAPSHOT");

	private final String text;

	private Version(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public static Version versionProperty(final String name) {
		return new Version("${" + name + "}");
	}

	public static Version plainVersion(final String version) {
		return new Version(version);
	}

	public static Version empty() {
		return EMPTY_VERSION;
	}

	public boolean isEmpty() {
		return this == EMPTY_VERSION;
	}

	@Override
	public int hashCode() {
		return Objects.hash(text);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Version other = (Version) obj;
		return Objects.equals(text, other.text);
	}
}
