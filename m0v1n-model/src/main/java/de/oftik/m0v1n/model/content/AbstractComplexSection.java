package de.oftik.m0v1n.model.content;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.oftik.m0v1n.model.contract.ComplexSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Section;
import de.oftik.m0v1n.model.contract.SubSection;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.content.Version;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * Template for a structured element.
 *
 * @author onkobu
 *
 */
public abstract class AbstractComplexSection extends AbstractSection implements ComplexSection, SubSection {
	private final List<SubSection> subSections;
	private final Plugin plugin;
	private Section parent;
	private ComplexSection complexParent;

	protected AbstractComplexSection(final String name, final Plugin p) {
		super(name);
		this.plugin = p;
		subSections = null;
	}

	protected AbstractComplexSection(final String name, final List<SubSection> subSections) {
		super(name);
		this.subSections = subSections;
		this.plugin = null;
		for (final SubSection ss : subSections) {
			ss.setParent(this);
		}
	}

	@Override
	public void setParent(final Section section) {
		this.parent = section;
	}

	@Override
	public void setParent(final ComplexSection parent) {
		this.complexParent = parent;
	}

	@Override
	public String getFullName() {
		return getParent() == null ? getName() : getParent().getName() + " - " + getName();
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
		// nothing to add
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		final Map<String, String> props = new HashMap<>();
		if (subSections == null) {
			return props;
		}
		for (final SubSection ss : subSections) {
			final Map<String, String> properties = ss.getProperties(ctx);
			if (properties == null || properties.isEmpty()) {
				continue;
			}
			props.putAll(properties);
		}
		return props;
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		super.requestInput(ctx);
		if (plugin != null) {
			final var suggestedVersion = plugin.getVersion(ctx.getVersionProperties());
			if (parent == null) {
				System.out.printf("%s (%s)%n", plugin.getFullName(), suggestedVersion);
			} else {
				System.out.printf("%s - %s (%s)%n", parent.getName(), plugin.getFullName(), suggestedVersion);
			}
			final String value = ctx.nextLine();
			if (value != null && !value.isBlank()) {
				plugin.setVersion(Version.plainVersion(value));
			} else {
				plugin.setVersion(suggestedVersion);
			}
			return;
		}
		for (final SubSection ss : subSections) {
			if (parent == null) {
				System.out.printf("%s%n", ss.getFullName());
			} else {
				System.out.printf("%s - %s%n", parent.getName(), ss.getFullName());
			}
			ss.requestInput(ctx);
		}
	}

	protected <T extends SubSection> Optional<T> getSection(final Class<T> cls) {
		if (plugin != null) {
			return Optional.empty();
		}
		for (final SubSection ss : subSections) {
			if (ss.getClass() == cls) {
				return Optional.of((T) ss);
			}
		}
		return Optional.empty();
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		super.writeTo(writer, indent, ctx);
		if (!isInputAvailable() && !isPredefined()) {
			return;
		}
		if (plugin != null) {
			plugin.writeTo(writer, indent, ctx);
			return;
		}

		for (final SubSection ss : subSections) {
			if (ss.getComment() != null) {
				PomXml.comment(writer, ss.getComment(), indent);
			}
			ss.writeTo(writer, indent, ctx);
		}
	}

	protected Section getParent() {
		return parent;
	}

	protected ComplexSection getComplexParent() {
		return complexParent;
	}
}
