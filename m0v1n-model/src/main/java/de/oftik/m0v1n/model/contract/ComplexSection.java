package de.oftik.m0v1n.model.contract;

/**
 * Contract for a structured element.
 *
 * @author onkobu
 *
 */
public interface ComplexSection extends Section {
	void setParent(Section section);
}
