package de.oftik.m0v1n.model.maven;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.PomDefinition;
import de.oftik.m0v1n.model.contract.Section;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.format.TabIndent;
import de.oftik.m0v1n.model.maven.content.GeneralSection;
import de.oftik.m0v1n.model.maven.content.GroupTagName;
import de.oftik.m0v1n.model.maven.content.InlineTagName;
import de.oftik.m0v1n.model.maven.content.Version;
import de.oftik.m0v1n.model.maven.particles.Comment;

/**
 * Represents a POM.
 *
 * @author onkobu
 *
 */
public class PomXml implements PomDefinition {
	private final List<Section> sections;

	public PomXml(final List<Section> sections) {
		this.sections = sections;
	}

	public static void newline(final Writer w) throws IOException {
		w.append("\n");
	}

	// Refactor: opening a tag yields a handle to either close it or dive deeper/
	// nest further.
	public static void openTag(final Writer w, final TagName tn, final Indent indent) throws IOException {
		indent.indent(w);
		w.append("<").append(tn.name()).append(">");
		if (tn.isGroup()) {
			indent.push();
			newline(w);
		}
	}

	public static void closeTag(final Writer w, final TagName tn, final Indent indent) throws IOException {
		if (tn.isGroup()) {
			indent.pop();
		}
		indent.indent(w);
		w.append("</").append(tn.name()).append(">");
		newline(w);
	}

	public static void writeValue(final Writer w, final InlineTagName tn, final String value, final Indent indent)
			throws IOException {
		indent.indent(w);
		openTag(w, tn, Indent.ZERO_INDENT);
		w.append(value);
		closeTag(w, tn, Indent.ZERO_INDENT);
	}

	public static void writeValue(final Writer w, final InlineTagName tn, final Version value, final Indent indent)
			throws IOException {
		writeValue(w, tn, value.toString(), indent);
	}

	public static void closeFreeInlineTag(final Writer w, final String tn) throws IOException {
		w.append("</").append(tn).append(">");
		newline(w);
	}

	public static void openFreeTag(final Writer w, final String tn, final boolean newline, final Indent indent)
			throws IOException {
		indent.indent(w);
		w.append("<").append(tn).append(">");
		if (newline) {
			newline(w);
			indent.push();
		}
	}

	public static void closeFreeGroupTag(final Writer w, final String tn, final Indent indent) throws IOException {
		indent.pop();
		indent.indent(w);
		w.append("</").append(tn).append(">");
		newline(w);
	}

	public static void writeFreeValue(final Writer w, final String tn, final String value, final Indent indent)
			throws IOException {
		indent.indent(w);
		openFreeTag(w, tn, false, Indent.ZERO_INDENT);
		w.append(value);
		closeFreeInlineTag(w, tn);
	}

	@Override
	public final void writeTo(final Writer writer, final MavenContext ctx) throws IOException {
		final Map<String, String> properties = new HashMap<>();
		// It is possible to register all VersionProperties here which writes all of
		// them, not only the selected ones.
		for (final Section s : sections) {
			final Map<String, String> sProps = s.getProperties(ctx);
			if (sProps == null || sProps.isEmpty()) {
				continue;
			}
			properties.putAll(sProps);
		}
		writeTo(writer, new TabIndent(), ctx, properties);
	}

	@Override
	public List<Section> getSections() {
		return sections;
	}

	@Override
	public final void start(final MavenContext ctx) throws IOException {

		for (final Section s : sections) {
			s.requestInput(ctx);
		}

		for (final Section s : sections) {
			s.adjustContext(ctx);
		}

		final Indent indent = new TabIndent();
		indent.push();
	}

	protected final void writeTo(final Writer writer, final Indent indent, final MavenContext ctx,
			final Map<String, String> properties) throws IOException {
		writer.append("<?xml version=\"1.0\"?>\n");
		writer.append("<project>\n");
		indent.push();
		for (final Section s : getSections()) {
			s.writeTo(writer, indent, ctx);
			if (s instanceof GeneralSection) {
				renderProperties(writer, properties, indent);
			}
		}
		indent.pop();
		writer.append("</project>\n");
	}

	private void renderProperties(final Writer w, final Map<String, String> properties, final Indent indent)
			throws IOException {
		if (properties.isEmpty()) {
			return;
		}
		PomXml.openTag(w, GroupTagName.properties, indent);
		for (final Entry<String, String> property : properties.entrySet()) {
			PomXml.openFreeTag(w, property.getKey(), false, indent);
			w.append(property.getValue());
			PomXml.closeFreeInlineTag(w, property.getKey());
		}
		PomXml.closeTag(w, GroupTagName.properties, indent);
	}

	public static void comment(final Writer w, final Comment comment, final Indent indent) throws IOException {
		comment(w, comment.getText(), indent);
	}

	public static void comment(final Writer w, final String string, final Indent indent) throws IOException {
		newline(w);
		indent.indent(w);
		w.append("<!--");
		w.append(string);
		w.append("-->");
		newline(w);
	}
}
