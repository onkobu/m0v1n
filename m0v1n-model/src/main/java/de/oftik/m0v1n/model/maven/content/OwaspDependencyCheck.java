package de.oftik.m0v1n.model.maven.content;

import java.util.EnumSet;

import de.oftik.m0v1n.model.maven.particles.Plugin;

public class OwaspDependencyCheck extends Plugin {
	public OwaspDependencyCheck() {
		this(Constants.OWASP_DEPENDENCY_CHECK_VERSION);
	}

	public OwaspDependencyCheck(final Version version) {
		super("org.owasp", "dependency-check-maven", version, null,
				EnumSet.of(SubSection.configuration, SubSection.executions));
		addExecution(new ExecutionForGoal(Goal.check));
	}
}
