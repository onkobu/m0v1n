package de.oftik.m0v1n.model.contract;

import java.util.Map;

import de.oftik.m0v1n.model.input.InputValue;

/**
 * Contract for a section in general.
 *
 * @author onkobu
 *
 */
public interface Section extends Writable {

	String getName();

	/**
	 * First stage, request input for all attributes.
	 */
	void requestInput(MavenContext ctx);

	/**
	 * Properties provided by this section. Invoked after input but before writing.
	 *
	 * @return
	 */
	Map<String, String> getProperties(MavenContext ctx);

	/**
	 * Set flags after input took place but before flushing of properties.
	 *
	 * @param ctx
	 */
	void adjustContext(MavenContext ctx);

	/**
	 * Lookup the value in the section's attribute and return in case it is found.
	 *
	 * @param vName
	 * @return <code>null</code> if not available/ undefined.
	 */
	InputValue findValue(String vName);

}
