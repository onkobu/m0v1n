package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import de.oftik.m0v1n.model.content.AbstractComplexSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Comment;

public class ModulesSection extends AbstractComplexSection {
	public ModulesSection(List<ModuleSection> modules) {
		super("Modules", new ArrayList<>(modules));
		setPredefined(true);
	}

	@Override
	public Comment getComment() {
		return null;
	}

	@Override
	public void writeTo(Writer writer, Indent indent, MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.modules, indent);
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.modules, indent);
	}

}
