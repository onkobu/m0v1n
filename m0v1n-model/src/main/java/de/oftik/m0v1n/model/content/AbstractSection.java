package de.oftik.m0v1n.model.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Section;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;

/**
 * Template for group of elements.
 *
 * @author onkobu
 *
 */
public abstract class AbstractSection implements Section {
	private final Collection<Attribute> attributes;
	private final Map<String, Attribute> attributeMap = new HashMap<>();
	private final String name;
	private boolean inputAvailable;
	private boolean predefined;

	protected AbstractSection(final String name, final Attribute... attributes) {
		this(name, Arrays.asList(attributes));
	}

	protected AbstractSection(final String name, final Collection<Attribute> attributes) {
		this.attributes = attributes;
		this.name = name;
		for (final Attribute attr : attributes) {
			attributeMap.put(attr.getName(), attr);
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public InputValue findValue(final String vName) {
		return attributeMap.containsKey(vName) ? attributeMap.get(vName).getInputValue() : InputValue.undefined();
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		return null;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		if (!isInputAvailable() && !isPredefined()) {
			return;
		}
		for (final Attribute attr : attributes) {
			attr.writeTo(writer, indent, ctx);
		}
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		for (final Attribute attr : attributes) {
			attr.requestInput(ctx);
		}
		inputAvailable = true;
	}

	public boolean isPredefined() {
		return predefined;
	}

	protected boolean isInputAvailable() {
		return inputAvailable;
	}

	protected void setPredefined(final boolean state) {
		this.predefined = state;
	}

	public Collection<Attribute> getAttributes() {
		return attributes;
	}
}
