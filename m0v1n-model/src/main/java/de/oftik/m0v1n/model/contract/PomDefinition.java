package de.oftik.m0v1n.model.contract;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * POM and context.
 *
 * @author onkobu
 *
 */
public interface PomDefinition {
	void start(MavenContext ctx) throws IOException;

	void writeTo(Writer writer, MavenContext ctx) throws IOException;

	List<Section> getSections();
}
