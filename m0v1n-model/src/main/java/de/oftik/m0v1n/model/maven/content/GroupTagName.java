package de.oftik.m0v1n.model.maven.content;

import de.oftik.m0v1n.model.maven.TagName;

public enum GroupTagName implements TagName {
	dependencyManagement,

	dependencies,

	modules,

	plugin,

	plugins,

	parent,

	properties,

	reporting,

	build,

	configuration,

	dependency,

	execution,

	executions,

	goals,

	sourceDirs,

	pluginManagement;

	@Override
	public boolean isGroup() {
		return true;
	}
}
