package de.oftik.m0v1n.model.maven;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Hides all writable methods of a regular Java Properties-class.
 *
 * @author onkobu
 *
 */
public final class VersionProperties {
	private static final VersionProperties INSTANCE = new VersionProperties();

	private final Properties properties;

	private VersionProperties() {
		this.properties = new Properties();
		try (InputStream pIn = ClassLoader.getSystemResourceAsStream("version.properties")) {
			this.properties.load(pIn);
		} catch (final IOException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * This is not in use for regular code.
	 *
	 * @see #instance()
	 * @param props
	 */
	public VersionProperties(final Properties props) {
		this.properties = props;
	}

	public String getOrDefault(final String key, final String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}

	public Map<String, String> map() {
		final Map<String, String> result = new HashMap<>();
		properties.forEach((key, value) -> result.put(String.valueOf(key), String.valueOf(value)));
		return Collections.unmodifiableMap(result);
	}

	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}

	public String get(final String key) {
		return properties.containsKey(key) ? properties.getProperty(key) : key;
	}

	public static VersionProperties instance() {
		return INSTANCE;
	}
}
