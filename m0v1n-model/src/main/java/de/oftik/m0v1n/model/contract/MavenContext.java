package de.oftik.m0v1n.model.contract;

import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.VersionProperties;
import de.oftik.m0v1n.model.maven.content.Version;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * Contract to control the context.
 *
 * @author onkobu
 *
 */
public interface MavenContext {
	enum Flag {
		PACKAGING_POM;
	}

	void setFlag(Flag flag);

	boolean isSet(Flag f);

	void forcePlugin(String artifactId, Version version, Comment comment);

	void forcePlugin(Plugin p);

	InputValue findValue(String string);

	String getVersionProperty(String key, String defaultValue);

	VersionProperties getVersionProperties();

	String nextLine();

	InputValue projectGroupId();

	InputValue projectArtifactId();
}
