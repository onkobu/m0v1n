package de.oftik.m0v1n.model.maven.content;

/**
 * Specific dependency.
 *
 * @author onkobu
 *
 */
public class DependencyApacheCli extends Dependency {
	public DependencyApacheCli() {
		super("commons-cli", "commons-cli", "Dependency Apache Common CLI", Version.plainVersion("1.4"));
	}
}
