package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import de.oftik.m0v1n.model.content.AbstractSubSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;
import de.oftik.m0v1n.model.maven.particles.Plugin.Configuration;
import de.oftik.m0v1n.model.maven.particles.Plugin.SubSection;

/**
 * Versions plugin.
 *
 * @author onkobu
 *
 */
public class VersionsMavenSection extends AbstractSubSection {
	private static final String VERSIONS_MAVEN_VERSION = "versions-maven.version";
	private final Plugin p = new Plugin("org.codehaus.mojo", "versions-maven-plugin",
			Version.versionProperty(VERSIONS_MAVEN_VERSION), null, EnumSet.of(SubSection.configuration))
					.addConfiguration(new Configuration() {
						@Override
						protected void writeContent(final Writer writer, final Indent indent) throws IOException {
							PomXml.writeFreeValue(writer, "generateBackupPoms", "false", indent);
						}
					});

	public VersionsMavenSection() {
		super("Versions Maven Plugin", new Comment("Run with mvn versions:display-plugin-updates"));
	}

	@Override
	public String getFullName() {
		return getComplexParent().getName() + " - " + getName();
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		final Map<String, String> versions = new HashMap<>();
		versions.put(VERSIONS_MAVEN_VERSION, ctx.getVersionProperty(VERSIONS_MAVEN_VERSION, "2.7"));
		return versions;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		p.writeTo(writer, indent, ctx);
	}
}
