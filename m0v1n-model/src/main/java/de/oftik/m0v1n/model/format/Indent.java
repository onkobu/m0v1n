package de.oftik.m0v1n.model.format;

import java.io.IOException;
import java.io.Writer;

/**
 * Level of indentation.
 *
 * @author onkobu
 *
 */
public abstract class Indent {

	public static Indent ZERO_INDENT = new Indent() {
		@Override
		public void indent(final Writer writer) throws IOException {
		}
	};

	private int indent;

	public void push() {
		indent++;
	}

	public void pop() {
		indent--;
	}

	public int getCurrentIndent() {
		return indent;
	}

	public abstract void indent(Writer writer) throws IOException;

}
