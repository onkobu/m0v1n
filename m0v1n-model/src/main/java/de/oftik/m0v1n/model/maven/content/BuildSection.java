package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

import de.oftik.m0v1n.model.content.AbstractComplexSection;
import de.oftik.m0v1n.model.contract.ComplexSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.SubSection;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * The &lt;build&gt;-section.
 *
 * @author onkobu
 *
 */
public class BuildSection extends AbstractComplexSection {

	public BuildSection() {
		super("Build", Arrays.asList(new PluginsSection(), new PluginManagementSection()));
	}

	public BuildSection(final List<SubSection> staticSections) {
		super("Build", staticSections);
		setPredefined(true);
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
		// intentionally left blank

	}

	@Override
	public void setParent(final ComplexSection parent) {
		throw new IllegalArgumentException(getName() + " cannot be part of " + parent);
	}

	public void forcePlugin(final Plugin plugin) {
		getSection(PluginsSection.class).ifPresent(ps -> ps.forcePlugin(plugin));
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.build, indent);
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.build, indent);
	}

	@Override
	public Comment getComment() {
		return null;
	}
}
