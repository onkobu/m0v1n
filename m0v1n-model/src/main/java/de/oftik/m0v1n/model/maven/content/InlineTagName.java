package de.oftik.m0v1n.model.maven.content;

import de.oftik.m0v1n.model.maven.TagName;

public enum InlineTagName implements TagName {
	packaging,

	module,

	artifactId,

	version,

	groupId,

	scope,

	id,

	goal,

	sourceDir,

	phase,

	modelVersion;

	@Override
	public boolean isGroup() {
		return false;
	}

}
