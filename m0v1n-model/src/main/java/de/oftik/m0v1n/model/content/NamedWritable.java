package de.oftik.m0v1n.model.content;

import java.util.Map;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Named;
import de.oftik.m0v1n.model.contract.Writable;

public abstract class NamedWritable implements Named, Writable {
	private final String name;

	protected NamedWritable(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	/**
	 * To define properties in pom.xml head section.
	 *
	 * @param properties
	 * @param ctx
	 */
	protected abstract void registerProperty(final Map<String, String> properties, final MavenContext ctx);
}
