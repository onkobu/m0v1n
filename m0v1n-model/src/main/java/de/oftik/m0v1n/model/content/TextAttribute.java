package de.oftik.m0v1n.model.content;

import java.util.Map;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.content.InlineTagName;

/**
 * Fixed text attribute.
 *
 * @author onkobu
 *
 */
public class TextAttribute extends AbstractAttribute {
	private InputValue value = InputValue.undefined();
	private final String example;

	public TextAttribute(final InlineTagName name) {
		this(name, null);
	}

	public TextAttribute(final InlineTagName name, final String example) {
		super(name);
		this.example = example;
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		if (example == null) {
			System.out.printf("%s: ", getName());
		} else {
			System.out.printf("%s (%s): ", getName(), example);
		}
		final String input = ctx.nextLine();
		if (input == null || input.isBlank()) {
			if (example != null) {
				value = InputValue.of(example);
			}
		} else {
			value = InputValue.of(input);
		}
	}

	@Override
	public InputValue getInputValue() {
		return value;
	}

	@Override
	public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {

	}
}
