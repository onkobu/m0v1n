package de.oftik.m0v1n.model.maven.content;

import static de.oftik.m0v1n.model.maven.content.Constants.ENFORCE_MAVEN_VERSION;

import java.io.IOException;
import java.io.Writer;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import de.oftik.m0v1n.model.content.AbstractSubSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;
import de.oftik.m0v1n.model.maven.particles.Plugin.Execution;
import de.oftik.m0v1n.model.maven.particles.Plugin.SubSection;

/**
 * Maven enforcer plugin.
 *
 * @author onkobu
 *
 */
public class MavenEnforcerSection extends AbstractSubSection {
	private static final String ENFORCER_VERSION = "enforcer.version";
	private final Plugin p = new Plugin("org.apache.maven.plugins", "maven-enforcer-plugin",
			Version.versionProperty(ENFORCER_VERSION), null, EnumSet.of(SubSection.executions))
					.addExecution(new Execution() {
						@Override
						protected void writeContent(final Writer writer, final Indent indent) throws IOException {
							PomXml.writeValue(writer, InlineTagName.id, "enforce-maven", indent);
							PomXml.openTag(writer, GroupTagName.goals, indent);
							PomXml.writeValue(writer, InlineTagName.goal, "enforce", indent);
							PomXml.closeTag(writer, GroupTagName.goals, indent);
							PomXml.openTag(writer, GroupTagName.configuration, indent);
							PomXml.openFreeTag(writer, "rules", true, indent);
							PomXml.openFreeTag(writer, "requireMavenVersion", true, indent);
							PomXml.writeValue(writer, InlineTagName.version, ENFORCE_MAVEN_VERSION, indent);
							PomXml.closeFreeGroupTag(writer, "requireMavenVersion", indent);
							PomXml.closeFreeGroupTag(writer, "rules", indent);
							PomXml.closeTag(writer, GroupTagName.configuration, indent);
						}
					});

	public MavenEnforcerSection() {
		super("Maven Enforcer", new Comment("Makes builds reliable by forcing plugin versions to a minimal version"));
	}

	@Override
	public String getFullName() {
		return getComplexParent().getName() + " - " + getName();
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		final Map<String, String> versions = new HashMap<>();
		versions.put(ENFORCER_VERSION, ctx.getVersionProperty(ENFORCER_VERSION, "3.0.0-M2"));
		return versions;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		p.writeTo(writer, indent, ctx);
	}
}
