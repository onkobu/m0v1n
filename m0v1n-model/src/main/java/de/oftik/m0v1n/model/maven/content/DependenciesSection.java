package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.oftik.kehys.lippu.Issue;
import de.oftik.kehys.lippu.Reason;
import de.oftik.kehys.lippu.Refactor;
import de.oftik.kehys.lippu.Severity;
import de.oftik.m0v1n.model.content.AbstractSection;
import de.oftik.m0v1n.model.content.YesNoAttribute;
import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.PomXml;

/**
 * &lt;dependencies&gt;
 *
 * @author onkobu
 *
 */
public class DependenciesSection extends AbstractSection {
	private static final List<Attribute> DEFAULT_DEPS = Arrays.asList(new DependencyLogging(), new DependencyJUnit(),
			new YesNoAttribute(new DependencyApacheCli()), new YesNoAttribute(new DependencyHamcrest()),
			new YesNoAttribute(new DependencyKotlin()));

	public enum Scope {
		test;
	}

	public DependenciesSection() {
		this(DEFAULT_DEPS);
	}

	@Refactor(@Issue(author = "onkobu", dateFound = "Nov.2021", reason = Reason.NOT_LISKOV_SUBSTITUE, severity = Severity.HIGH, value = "Any attribute could be put in, not only dependencies"))
	public DependenciesSection(final Attribute... attributes) {
		this(Arrays.asList(attributes));
	}

	public DependenciesSection(final Collection<Attribute> attributes) {
		super("dependencies", attributes);

		setPredefined(true);
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
	}

	@Override
	public InputValue findValue(final String vName) {
		return null;
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		final Map<String, String> properties = new HashMap<>();
		for (final Attribute attr : getAttributes()) {
			attr.registerProperty(properties, ctx);
		}
		return properties;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.dependencies, indent);
		writer.append("\n");
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.dependencies, indent);
		writer.append("\n");
	}

	static void appendDependency(final Writer w, final String groupId, final String artifactId, final Version version,
			final Scope scope, final Indent indent) throws IOException {
		PomXml.openTag(w, GroupTagName.dependency, indent);
		PomXml.writeValue(w, InlineTagName.groupId, groupId, indent);
		PomXml.writeValue(w, InlineTagName.artifactId, artifactId, indent);
		if (!version.isEmpty()) {
			PomXml.writeValue(w, InlineTagName.version, version.toString(), indent);
		}
		if (scope != null) {
			PomXml.writeValue(w, InlineTagName.scope, scope.name(), indent);
		}
		PomXml.closeTag(w, GroupTagName.dependency, indent);
	}
}
