package de.oftik.m0v1n.model.maven.content;

import static de.oftik.m0v1n.model.maven.content.JaCoCoSection.LimitAttribute.limit;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.content.AbstractSubSection;
import de.oftik.m0v1n.model.content.ConstantAttribute;
import de.oftik.m0v1n.model.content.PercentAttribute;
import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.PomXml;

/**
 * JaCoCo plugin.
 *
 * @author onkobu
 *
 */
public class JaCoCoSection extends AbstractSubSection {
	private static final double BRANCH_COVERAGE = 0.85;
	private static final double INSTRUCTION_COVERAGE = 0.85;
	private static final double LINE_COVERAGE = 0.80;
	private static final double METHOD_COVERAGE = 0.85;

	public enum Limit {
		INSTRUCTION(new PercentAttribute("instructionCoverage", INSTRUCTION_COVERAGE)),

		BRANCH(new PercentAttribute("branchCoverage", BRANCH_COVERAGE)),

		LINE(new PercentAttribute("lineCoverage", LINE_COVERAGE)),

		METHOD(new PercentAttribute("methodCoverage", METHOD_COVERAGE));

		private final LimitAttribute attribute;

		Limit(final PercentAttribute attribute) {
			this.attribute = new LimitAttribute(this, attribute);
		}
	}

	public static final JaCoCoSection JACOCO_WITH_DEFAULTS = new JaCoCoSection(limit(Limit.BRANCH, BRANCH_COVERAGE),
			limit(Limit.INSTRUCTION, INSTRUCTION_COVERAGE), limit(Limit.LINE, LINE_COVERAGE),
			limit(Limit.METHOD, METHOD_COVERAGE));

	public JaCoCoSection() {
		super("JaCoCo", Limit.BRANCH.attribute, Limit.INSTRUCTION.attribute, Limit.LINE.attribute,
				Limit.METHOD.attribute);
	}

	JaCoCoSection(final LimitAttribute branchLimit, final LimitAttribute instructionLimit,
			final LimitAttribute lineLimit, final LimitAttribute methodLimit) {
		super("JaCoCo", branchLimit, instructionLimit, lineLimit, methodLimit);
		setPredefined(true);
	}

	@Override
	public String getFullName() {
		return getComplexParent().getName() + " - " + getName();
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
// intentionally empty
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.plugin, indent);
		PomXml.writeValue(writer, InlineTagName.groupId, "org.jacoco", indent);
		PomXml.writeValue(writer, InlineTagName.artifactId, "jacoco-maven-plugin", indent);
		PomXml.writeValue(writer, InlineTagName.version, "0.8.4", indent);
		PomXml.openTag(writer, GroupTagName.configuration, indent);

		PomXml.openFreeTag(writer, "rules", true, indent);
		PomXml.openFreeTag(writer, "rule implementation=\"org.jacoco.maven.RuleConfiguration\"", true, indent);
		PomXml.writeFreeValue(writer, "element", "BUNDLE", indent);

		PomXml.openFreeTag(writer, "limits", true, indent);
		super.writeTo(writer, indent, ctx);
		PomXml.closeFreeGroupTag(writer, "limits", indent);

		PomXml.closeFreeGroupTag(writer, "rule", indent);
		PomXml.closeFreeGroupTag(writer, "rules", indent);

		PomXml.closeTag(writer, GroupTagName.configuration, indent);
		PomXml.closeTag(writer, GroupTagName.plugin, indent);
	}

	static class LimitAttribute implements Attribute {
		private final Limit limit;
		private final Attribute outer;

		LimitAttribute(final Limit name, final Attribute outer) {
			super();
			this.limit = name;
			this.outer = outer;
		}

		@Override
		public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
			PomXml.openFreeTag(writer, "limit implementation=\"org.jacoco.report.check.Limit\"", true, indent);
			PomXml.writeFreeValue(writer, "counter", limit.name(), indent);
			PomXml.writeFreeValue(writer, "value", "COVEREDRATIO", indent);
			PomXml.writeFreeValue(writer, "minimum", outer.getInputValue().get(), indent);
			PomXml.closeFreeGroupTag(writer, "limit", indent);
		}

		static LimitAttribute limit(final Limit l, final double value) {
			return new LimitAttribute(l, new ConstantAttribute(value));
		}

		@Override
		public void requestInput(final MavenContext ctx) {
			if (outer instanceof ConstantAttribute) {
				throw new UnsupportedOperationException();
			}
			outer.requestInput(ctx);
		}

		@Override
		public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
			if (outer instanceof ConstantAttribute) {
				throw new UnsupportedOperationException();
			}
			outer.registerProperty(properties, ctx);
		}

		@Override
		public String getName() {
			return outer.getName();
		}

		@Override
		public InputValue getInputValue() {
			return outer.getInputValue();
		}

	}
}
