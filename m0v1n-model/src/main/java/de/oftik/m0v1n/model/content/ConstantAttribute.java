package de.oftik.m0v1n.model.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.content.InlineTagName;

/**
 * An value with a name that does not change over time.
 *
 * @author onkobu
 *
 */
public class ConstantAttribute implements Attribute {
	private final String name;
	private final InputValue value;

	public ConstantAttribute(final double value) {
		this(String.valueOf(value));
	}

	public ConstantAttribute(final String value) {
		this.name = null;
		this.value = new InputValue(value);
	}

	public ConstantAttribute(final InlineTagName name, final String value) {
		super();
		this.name = name.name();
		this.value = new InputValue(value);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.writeFreeValue(writer, getName(), value.get(), indent);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		throw new UnsupportedOperationException("constant attribute");
	}

	@Override
	public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
	}

	@Override
	public InputValue getInputValue() {
		return value;
	}
}
