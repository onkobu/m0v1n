package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.content.ChoiceAttribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Named;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.content.JavaVersion.JDK;

/**
 * Meta information about java version.
 *
 * @author onkobu
 *
 */
public class JavaVersion extends ChoiceAttribute<JDK> {
	public enum JDK implements Named {
		v8("1.8"),

		v11("11"),

		v12("12"),

		v13("13"),

		v14("14");

		String name;

		JDK(final String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}

		void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
			properties.put("java.version", name);
		}
	}

	public JavaVersion() {
		super("JDK Version", 1, JDK.values());
	}

	public JavaVersion(final JDK jdk) {
		super("JDK Version", jdk);
	}

	@Override
	public InputValue getInputValue() {
		return InputValue.of(getChoiceValue().name());
	}

	@Override
	public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
		getChoiceValue().registerProperty(properties, ctx);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		ctx.forcePlugin(new CompilerPlugin().withJDKVersionReference());
	}
}
