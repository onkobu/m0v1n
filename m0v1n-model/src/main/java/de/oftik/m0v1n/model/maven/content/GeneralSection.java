package de.oftik.m0v1n.model.maven.content;

import static de.oftik.m0v1n.model.maven.content.InlineTagName.packaging;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import de.oftik.m0v1n.model.content.AbstractSection;
import de.oftik.m0v1n.model.content.ConstantAttribute;
import de.oftik.m0v1n.model.content.TextAttribute;
import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.MavenContext.Flag;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;

/**
 * The header part of the POM with meta information.
 *
 * @author onkobu
 *
 */
public class GeneralSection extends AbstractSection {

	private static final ConstantAttribute MODEL_VERSION_ATTRIBUTE = new ConstantAttribute(InlineTagName.modelVersion,
			"4.0.0");
	private final GeneralSection parent;

	public GeneralSection() {
		super("project", new TextAttribute(InlineTagName.modelVersion, "4.0.0"),
				new TextAttribute(InlineTagName.groupId), new TextAttribute(InlineTagName.artifactId),
				new TextAttribute(InlineTagName.version, "1.0.0-SNAPSHOT"), new JavaVersion());
		parent = null;
	}

	/**
	 * General section of a parent POM.
	 *
	 * @param groupId
	 * @param artifactId
	 * @param version
	 * @param jdk
	 */
	public GeneralSection(final String groupId, final String artifactId, final String version, final JavaVersion jdk) {
		super("project", MODEL_VERSION_ATTRIBUTE, new ConstantAttribute(InlineTagName.groupId, groupId),
				new ConstantAttribute(InlineTagName.artifactId, artifactId),
				new ConstantAttribute(InlineTagName.version, version), jdk);
		parent = null;
		setPredefined(true);
	}

	/**
	 * Copy of its parent for a sub module.
	 *
	 * @param parent
	 */
	public GeneralSection(final GeneralSection parent, final String artifactId) {
		// version is taken from parent implicitly through Maven.
		super("project", MODEL_VERSION_ATTRIBUTE, new ConstantAttribute(InlineTagName.groupId, parent.getGroupId()),
				new ConstantAttribute(InlineTagName.artifactId, artifactId));
		setPredefined(true);
		this.parent = parent;
	}

	private String getVersion() {
		return findValue(InlineTagName.version.name()).get();
	}

	private String getGroupId() {
		return findValue(InlineTagName.groupId.name()).get();
	}

	private String getArtifactId() {
		return findValue(InlineTagName.artifactId.name()).get();
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
// intentionally empty
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		final Map<String, String> properties = new HashMap<>();
		for (final Attribute a : getAttributes()) {
			a.registerProperty(properties, ctx);
		}
		return properties;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.newline(writer);
		super.writeTo(writer, indent, ctx);
		if (parent != null) {
			PomXml.openTag(writer, GroupTagName.parent, indent);
			PomXml.writeValue(writer, InlineTagName.groupId, parent.getGroupId(), indent);
			PomXml.writeValue(writer, InlineTagName.artifactId, parent.getArtifactId(), indent);
			PomXml.writeValue(writer, InlineTagName.version, parent.getVersion(), indent);
			PomXml.closeTag(writer, GroupTagName.parent, indent);
		}
		if (ctx.isSet(Flag.PACKAGING_POM)) {
			PomXml.writeValue(writer, packaging, "pom", indent);
		}

		PomXml.newline(writer);
	}

}
