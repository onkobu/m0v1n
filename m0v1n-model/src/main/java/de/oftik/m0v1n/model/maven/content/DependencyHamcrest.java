package de.oftik.m0v1n.model.maven.content;

/**
 * Specific dependency.
 *
 * @author onkobu
 *
 */
public class DependencyHamcrest extends TestDependency {
	private static final String PROPERTY_HAMCREST_VERSION = "version.hamcrest";

	public DependencyHamcrest() {
		super("org.hamcrest", "hamcrest", "Enable Hamcrest Matchers", PROPERTY_HAMCREST_VERSION,
				Version.plainVersion("2.1"));
	}
}
