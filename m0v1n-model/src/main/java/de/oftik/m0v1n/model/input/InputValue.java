package de.oftik.m0v1n.model.input;

import java.util.function.Supplier;

/**
 * User input value.
 *
 * @author onkobu
 *
 */
public class InputValue {
	public static final InputValue NO_VALUE = new InputValue("false");

	public static final InputValue YES_VALUE = new InputValue("true");

	private final String value;
	private final Supplier<String> valueSupplier;

	public InputValue(final String value) {
		super();
		this.value = value;
		this.valueSupplier = null;
	}

	InputValue(final Supplier<String> valueSupplier) {
		this.value = null;
		this.valueSupplier = valueSupplier;
	}

	public String get() {
		return valueSupplier == null ? value : valueSupplier.get();
	}

	public static InputValue undefined() {
		return new InputValue(() -> {
			throw new IllegalStateException("is undefined");
		});
	}

	public static InputValue of(final String value) {
		return new InputValue(value);
	}
}
