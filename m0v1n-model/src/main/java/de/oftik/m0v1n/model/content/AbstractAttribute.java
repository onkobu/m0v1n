package de.oftik.m0v1n.model.content;

import java.io.IOException;
import java.io.Writer;

import de.oftik.m0v1n.model.contract.Attribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.content.InlineTagName;

/**
 * Template to assign a value to a name.
 *
 * @author onkobu
 */
public abstract class AbstractAttribute implements Attribute {
	private final String name;

	public AbstractAttribute(final InlineTagName name) {
		super();
		this.name = name.name();
	}

	public AbstractAttribute(final String name) {
		this.name = name;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.writeFreeValue(writer, getName(), getInputValue().get(), indent);
	}

	@Override
	public String getName() {
		return name;
	}
}
