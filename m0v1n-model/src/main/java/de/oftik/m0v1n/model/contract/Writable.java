package de.oftik.m0v1n.model.contract;

import java.io.IOException;
import java.io.Writer;

import de.oftik.m0v1n.model.format.Indent;

/**
 * Something that can be written.
 *
 * @author onkobu
 *
 */
public interface Writable {
	/**
	 * Finalizing by writing content.
	 *
	 * @param writer
	 * @param ctx
	 * @throws IOException
	 */
	void writeTo(Writer writer, Indent indent, MavenContext ctx) throws IOException;

}
