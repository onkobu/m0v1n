package de.oftik.m0v1n.model.format;

import java.io.IOException;
import java.io.Writer;

/**
 * Indentation with tabs.
 *
 * @author onkobu
 *
 */
public class TabIndent extends Indent {
	private static final String INDENTATION = "\t\t\t\t\t\t\t\t\t\t";

	@Override
	public void indent(final Writer writer) throws IOException {
		writer.append(INDENTATION.substring(0, getCurrentIndent()));
	}

}
