package de.oftik.m0v1n.model.maven;

/**
 * Contract for a tag name.
 *
 * @author onkobu
 *
 */
public interface TagName {
	String name();

	boolean isGroup();
}
