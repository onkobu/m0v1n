package de.oftik.m0v1n.model.maven.content;

import java.util.EnumSet;

import de.oftik.m0v1n.model.maven.particles.Plugin;

public class CheckstylePlugin extends Plugin {
	public CheckstylePlugin() {
		this(Version.EMPTY_VERSION);
	}

	public CheckstylePlugin(final Version version) {
		super("org.apache.maven.plugins", "maven-checkstyle-plugin", version, null,
				EnumSet.of(SubSection.configuration, SubSection.dependencies));
		addDependency(new Dependency("@groupId", "@artifactId-build-tools", Dependency.PROJECT_BUILDTOOLS_NAME,
				Version.empty()));
	}
}
