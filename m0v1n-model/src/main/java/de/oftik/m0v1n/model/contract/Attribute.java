package de.oftik.m0v1n.model.contract;

import java.util.Map;

import de.oftik.m0v1n.model.input.InputValue;

/**
 * Contract for an attribute.
 *
 * @author onkobu
 *
 */
public interface Attribute extends Writable {

	void requestInput(MavenContext ctx);

	void registerProperty(Map<String, String> properties, MavenContext ctx);

	String getName();

	InputValue getInputValue();

}
