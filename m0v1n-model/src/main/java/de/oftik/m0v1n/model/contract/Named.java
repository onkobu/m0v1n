package de.oftik.m0v1n.model.contract;

/**
 * Contract for a named element.
 *
 * @author onkobu
 *
 */
public interface Named {
	String getName();
}
