package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.oftik.m0v1n.model.content.NamedWritable;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.content.DependenciesSection.Scope;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * A single &lt;dependency&lt;-element.
 *
 * @author onkobu
 *
 */
public class Dependency extends NamedWritable {
	/**
	 * Name of the dependency that refers to the project's own build-tools
	 * submodule.
	 */
	public static final String PROJECT_BUILDTOOLS_NAME = "project-build-tools";

	private final String versionProperty;
	private final Version version;
	private final String groupId;
	private final String artifactId;
	private final Scope scope;
	private final List<Plugin> plugins;

	public Dependency(final String groupId, final String artifactId, final String name, final Version version) {
		this(groupId, artifactId, name, "version." + artifactId, version, null);
	}

	public Dependency(final String groupId, final String artifactId, final String name, final String versionProperty,
			final Version version) {
		this(groupId, artifactId, name, versionProperty, version, null);
	}

	public Dependency(final String groupId, final String artifactId, final String name, final String versionProperty,
			final Version version, final Scope scope, final Plugin... plugins) {
		super(name);
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.versionProperty = versionProperty;
		this.version = version;
		this.scope = scope;
		if (plugins == null) {
			this.plugins = Collections.emptyList();
		} else {
			this.plugins = Arrays.stream(plugins).collect(Collectors.toList());
		}
	}

	@Override
	protected final void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
		if (version.isEmpty()) {
			return;
		}
		properties.put(versionProperty, ctx.getVersionProperty(versionProperty, version.toString()));
	}

	@Override
	public final void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		final String gId;
		if ("@groupId".equals(groupId)) {
			gId = ctx.projectGroupId().get();
		} else {
			gId = groupId;
		}
		final String aId;
		if (artifactId != null && artifactId.startsWith("@artifactId")) {
			aId = artifactId.replace("@artifactId", ctx.projectArtifactId().get());
		} else {
			aId = artifactId;
		}
		DependenciesSection.appendDependency(writer, gId, aId,
				version.isEmpty() ? version : Version.versionProperty(versionProperty), scope, indent);
		plugins.stream().forEach(ctx::forcePlugin);
	}

}
