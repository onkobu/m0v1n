package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import de.oftik.m0v1n.model.content.AbstractSubSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Plugin;
import de.oftik.m0v1n.model.maven.particles.Plugin.Execution;
import de.oftik.m0v1n.model.maven.particles.Plugin.SubSection;

/**
 * PMD source code checker.
 *
 * @author onkobu
 *
 */
public class PmdSection extends AbstractSubSection {
	private static final String PMD_VERSION = "pmd.version";
	private final Plugin p = new Plugin("org.apache.maven.plugins", "maven-pmd-plugin",
			Version.versionProperty(PMD_VERSION), null, EnumSet.of(SubSection.executions))
					.addExecution(new Execution() {
						@Override
						protected void writeContent(final Writer writer, final Indent indent) throws IOException {
							PomXml.openTag(writer, GroupTagName.goals, indent);
							PomXml.writeValue(writer, InlineTagName.goal, "check", indent);
							PomXml.closeTag(writer, GroupTagName.goals, indent);
						}
					});

	public PmdSection() {
		super("PMD");
	}

	@Override
	public String getFullName() {
		return getComplexParent().getName() + " - " + getName();
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		final Map<String, String> versions = new HashMap<>();
		versions.put(PMD_VERSION, ctx.getVersionProperty(PMD_VERSION, "3.13.0"));
		return versions;
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		p.writeTo(writer, indent, ctx);
	}
}
