package de.oftik.m0v1n.model.content;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.SubSection;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * A section that can be skipped or which is optional.
 *
 * @author onkobu
 *
 */
public class SkippableSection extends AbstractComplexSection {
	private boolean skipped;

	public SkippableSection(final String name, final Plugin p) {
		super(name, p);
	}

	public SkippableSection(final String name, final List<SubSection> sections) {
		super(name, sections);
	}

	@Override
	public void adjustContext(final MavenContext ctx) {

	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		return skipped ? Collections.emptyMap() : super.getProperties(ctx);
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		System.out.printf("Configure %s Y/(N): ", getName());
		final String value = ctx.nextLine();
		if ("y".equalsIgnoreCase(value)) {
			super.requestInput(ctx);
		} else {
			skipped = true;
		}
	}

	@Override
	public Comment getComment() {
		return null;
	}

}
