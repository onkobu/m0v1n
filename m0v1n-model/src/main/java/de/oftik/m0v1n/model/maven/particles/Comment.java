package de.oftik.m0v1n.model.maven.particles;

import java.io.IOException;
import java.io.Writer;

public class Comment {
	private final String text;

	public Comment(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void writeTo(Writer w) throws IOException {
		w.append("<!-- ");
		w.append(text);
		w.append("-->\n");
	}

}
