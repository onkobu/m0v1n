package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

import de.oftik.m0v1n.model.content.AbstractComplexSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.SubSection;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Comment;

public class ReportingSection extends AbstractComplexSection {
	public ReportingSection() {
		super("Reporting", Arrays.asList(new PluginsSection()));
	}

	public ReportingSection(final List<SubSection> staticSections) {
		super("Reporting", staticSections);
		setPredefined(true);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.reporting, indent);
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.reporting, indent);
	}

	@Override
	public Comment getComment() {
		return null;
	}
}
