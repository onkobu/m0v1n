package de.oftik.m0v1n.model.maven.content;

import de.oftik.m0v1n.model.content.AbstractSubSection;
import de.oftik.m0v1n.model.content.ConstantAttribute;
import de.oftik.m0v1n.model.contract.MavenContext;

public class ModuleSection extends AbstractSubSection {
	public ModuleSection(String moduleName) {
		super("Module", new ConstantAttribute(InlineTagName.module, moduleName));
		setPredefined(true);
	}

	@Override
	public String getFullName() {
		return getComplexParent().getName() + " - " + getName();
	}

	@Override
	public void adjustContext(MavenContext ctx) {
		// intentionally empty
	}

}
