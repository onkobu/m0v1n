package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.content.ChoiceAttribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Named;
import de.oftik.m0v1n.model.contract.Writable;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.content.DependenciesSection.Scope;
import de.oftik.m0v1n.model.maven.content.DependencyJUnit.JUnit;
import de.oftik.m0v1n.model.maven.particles.Comment;

/**
 * Specific dependency.
 *
 * @author onkobu
 *
 */
public class DependencyJUnit extends ChoiceAttribute<JUnit> {
	public enum JUnit implements Named, Writable {
		None {

			@Override
			public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {

			}

			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {

			}
		},
		JUnit4 {
			@Override
			public void writeTo(final Writer w, final Indent indent, final MavenContext ctx) throws IOException {
				DependenciesSection.appendDependency(w, "junit", "junit",
						Version.versionProperty(PROPERTY_JUNIT4_VERSION), Scope.test, indent);
			}

			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
				properties.put(PROPERTY_JUNIT4_VERSION, ctx.getVersionProperty(PROPERTY_JUNIT4_VERSION, "4.12"));
			}
		},

		JUnit5 {
			@Override
			public void writeTo(final Writer w, final Indent indent, final MavenContext ctx) throws IOException {
				DependenciesSection.appendDependency(w, "org.junit.jupiter", "junit-jupiter-api",
						Version.versionProperty(PROPERTY_JUNIT5_VERSION), Scope.test, indent);
				DependenciesSection.appendDependency(w, "org.junit.jupiter", "junit-jupiter-engine",
						Version.versionProperty(PROPERTY_JUNIT5_VERSION), Scope.test, indent);
			}

			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
				properties.put(PROPERTY_JUNIT5_VERSION, ctx.getVersionProperty(PROPERTY_JUNIT5_VERSION, "5.6.0"));
			}
		},

		JUnit5Plus4 {
			@Override
			public void writeTo(final Writer w, final Indent indent, final MavenContext ctx) throws IOException {
				JUnit5.writeTo(w, indent, ctx);
				JUnit4.writeTo(w, indent, ctx);
				ctx.forcePlugin("maven-surefire-plugin", Version.versionProperty(PROPERTY_SUREFIRE_VERSION),
						new Comment("junit-jupiter runs JUnit4 and -5 correctly when surefire >=2.22.0 "));
			}

			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
				JUnit4.registerProperty(properties, ctx);
				JUnit5.registerProperty(properties, ctx);
				properties.put(PROPERTY_SUREFIRE_VERSION, ctx.getVersionProperty(PROPERTY_SUREFIRE_VERSION, "2.22.0"));
			}
		};

		private static final String PROPERTY_JUNIT4_VERSION = "version.junit4";
		private static final String PROPERTY_JUNIT5_VERSION = "version.junit5";
		private static final String PROPERTY_SUREFIRE_VERSION = "version.surefire";

		abstract void registerProperty(Map<String, String> properties, MavenContext ctx);

		@Override
		public String getName() {
			return name();
		}
	}

	public DependencyJUnit() {
		super("JUnit flavor", 0, JUnit.values());
	}

	@Override
	public InputValue getInputValue() {
		return InputValue.of(getChoiceValue().name());
	}

	@Override
	public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
		getChoiceValue().registerProperty(properties, ctx);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		getChoiceValue().writeTo(writer, indent, ctx);
	}
}
