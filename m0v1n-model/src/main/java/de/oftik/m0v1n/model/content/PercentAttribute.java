package de.oftik.m0v1n.model.content;

import java.util.Map;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.input.InputValue;

/**
 * Attribute with fraction representing percent.
 *
 * @author onkobu
 *
 */
public class PercentAttribute extends AbstractAttribute {
	private final Double example;
	private InputValue value;

	public PercentAttribute(final String attrName, final Double example) {
		super(attrName);
		this.example = example;
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		if (example == null) {
			System.out.printf("%s:", getName());
		} else {
			System.out.printf("%s (%.2f):", getName(), example);
		}
		final String input = ctx.nextLine();
		if (input == null || input.isBlank()) {
			if (example != null) {
				value = InputValue.of(String.format("%.2f", example));
			}
		} else {
			value = InputValue.of(String.format("%.2f", Double.parseDouble(input)));
		}

	}

	@Override
	public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {

	}

	@Override
	public InputValue getInputValue() {
		return value;
	}
}
