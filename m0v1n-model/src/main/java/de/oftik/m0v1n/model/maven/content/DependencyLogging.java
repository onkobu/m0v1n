package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.content.ChoiceAttribute;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Named;
import de.oftik.m0v1n.model.contract.Writable;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.content.DependencyLogging.Logging;

/**
 * Specific dependency.
 *
 * @author onkobu
 *
 */
public class DependencyLogging extends ChoiceAttribute<Logging> {
	private static final String LOG4J_VERSION = "log4j.version";
	private static final String SLF4J_VERSION = "slf4j.version";
	private static final String LOGBACK_VERSION = "logback.version";

	public enum Logging implements Named, Writable {
		JavaUtil() {
			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
			}

			@Override
			public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
				PomXml.comment(writer, "default logging through java.util is in use", indent);
			}
		},

		Log4J() {
			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
				properties.put(LOG4J_VERSION, ctx.getVersionProperties().get(LOG4J_VERSION));
			}

			@Override
			public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
				DependenciesSection.appendDependency(writer, "org.apache.logging.log4j", "log4j-api",
						Version.versionProperty(LOG4J_VERSION), null, indent);
				DependenciesSection.appendDependency(writer, "org.apache.logging.log4j", "log4j-core",
						Version.versionProperty(LOG4J_VERSION), null, indent);
			}
		},

		SLF4JLog4J() {
			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
				properties.put(LOG4J_VERSION, ctx.getVersionProperties().get(LOG4J_VERSION));
				properties.put(SLF4J_VERSION, ctx.getVersionProperties().get(SLF4J_VERSION));
			}

			@Override
			public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
				DependenciesSection.appendDependency(writer, "org.slf4j", "slf4j-api",
						Version.versionProperty(SLF4J_VERSION), null, indent);
				DependenciesSection.appendDependency(writer, "org.apache.logging.log4j", "log4j-slf4j18-impl",
						Version.versionProperty(LOG4J_VERSION), null, indent);
			}
		},

		SLF4JLogback() {
			@Override
			void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
				properties.put(LOGBACK_VERSION, ctx.getVersionProperties().get(LOGBACK_VERSION));
				properties.put(SLF4J_VERSION, ctx.getVersionProperties().get(SLF4J_VERSION));
			}

			@Override
			public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
				DependenciesSection.appendDependency(writer, "org.slf4j", "slf4j-api",
						Version.versionProperty(SLF4J_VERSION), null, indent);
				DependenciesSection.appendDependency(writer, "ch.qos.logback", "logback-classic",
						Version.versionProperty(LOGBACK_VERSION), null, indent);
			}
		};

		@Override
		public String getName() {
			return name();
		}

		abstract void registerProperty(Map<String, String> properties, MavenContext ctx);
	}

	public DependencyLogging() {
		super("Logging", 0, Logging.values());
	}

	public DependencyLogging(final Logging l) {
		super("Logging", l);
	}

	@Override
	public InputValue getInputValue() {
		return InputValue.of(getChoiceValue().name());
	}

	@Override
	public void registerProperty(final Map<String, String> properties, final MavenContext ctx) {
		getChoiceValue().registerProperty(properties, ctx);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		getChoiceValue().writeTo(writer, indent, ctx);
	}

}
