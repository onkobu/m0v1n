/**
 * Collection of contracts or API-elements. None of them contains code. There
 * only purpose is to organize communication and form interfaces between
 * components.
 */
package de.oftik.m0v1n.model.contract;
