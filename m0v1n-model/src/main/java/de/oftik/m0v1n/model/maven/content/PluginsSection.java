package de.oftik.m0v1n.model.maven.content;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import de.oftik.m0v1n.model.content.SkippableSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * &lt;plugins&gt;
 *
 * @author onkobu
 *
 */
public class PluginsSection extends SkippableSection {
	private final List<Plugin> plugins = new ArrayList<>();

	public PluginsSection() {
		super("Plugins",
				Arrays.asList(new MavenEnforcerSection(), new VersionsMavenSection(),
						new SkippableSection("enable JaCoCo Code Coverage", Arrays.asList(new JaCoCoSection())),
						new SkippableSection("enable PMD code analysis", Arrays.asList(new PmdSection())),
						new SkippableSection("enable OWASP dependency check", new OwaspDependencyCheck()),
						new SkippableSection("enable Checkstyle static code analysis", new CheckstylePlugin())));
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		PomXml.openTag(writer, GroupTagName.plugins, indent);
		for (final Plugin p : plugins) {
			p.writeTo(writer, indent, ctx);
		}
		super.writeTo(writer, indent, ctx);
		PomXml.closeTag(writer, GroupTagName.plugins, indent);
	}

	public void forcePlugin(final Plugin plugin) {
		final Optional<Plugin> merged = plugins.stream().filter(p -> p.samePlugin(plugin)).findFirst()
				.map(found -> found.merge(plugin));
		if (merged.isEmpty()) {
			plugins.add(plugin);
		}
	}
}
