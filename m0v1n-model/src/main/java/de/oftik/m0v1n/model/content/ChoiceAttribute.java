package de.oftik.m0v1n.model.content;

import java.util.Arrays;
import java.util.List;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Named;

/**
 * An assignment of a single value from a group of values to a name.
 *
 * @author onkobu
 *
 * @param <T>
 */
public abstract class ChoiceAttribute<T extends Named> extends AbstractAttribute {
	private final List<T> choices;
	private int choiceIndex;
	private T choiceValue;
	private int defaultChoice;

	protected ChoiceAttribute(final String name, final int defChoice, final T... choices) {
		super(name);
		this.choices = Arrays.asList(choices);
		this.defaultChoice = defChoice;
	}

	protected ChoiceAttribute(final String name, final T defValue) {
		super(name);
		choiceValue = defValue;
		defaultChoice = -1;
		choices = null;
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		System.out.printf("%s:%n", getName());
		for (int i = 0; i < choices.size(); i++) {
			System.out.printf("%d %s%n", i + 1, choices.get(i));
		}
		System.out.printf("Your choice (%s): ", choices.get(defaultChoice));
		final String line = ctx.nextLine();
		if (line == null || line.isEmpty()) {
			this.choiceIndex = defaultChoice;
			this.choiceValue = choices.get(defaultChoice);
		} else {
			final int value = Integer.parseInt(line) - 1;
			this.choiceIndex = value;
			this.choiceValue = choices.get(value);
		}
	}

	public List<T> getChoices() {
		return choices;
	}

	public int getChoiceIndex() {
		return choiceIndex;
	}

	public T getChoiceValue() {
		return choiceValue;
	}
}
