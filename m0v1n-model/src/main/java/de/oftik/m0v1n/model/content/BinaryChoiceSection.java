package de.oftik.m0v1n.model.content;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.Section;
import de.oftik.m0v1n.model.format.Indent;
import de.oftik.m0v1n.model.input.InputValue;

/**
 * Select one of two choices.
 *
 * @author onkobu
 *
 */
public class BinaryChoiceSection extends AbstractSection {
	private final String choice;
	private final Section yesSection;
	private final Section noSection;
	private Section activeSection;

	public BinaryChoiceSection(final String choice, final Section yesSection, final Section noSection) {
		super(choice);
		this.choice = choice;
		this.yesSection = yesSection;
		this.noSection = noSection;
	}

	@Override
	public void adjustContext(final MavenContext ctx) {
		activeSection.adjustContext(ctx);
	}

	@Override
	public InputValue findValue(final String vName) {
		return activeSection.findValue(vName);
	}

	@Override
	public Map<String, String> getProperties(final MavenContext ctx) {
		return activeSection.getProperties(ctx);
	}

	@Override
	public void requestInput(final MavenContext ctx) {
		System.out.printf("%s Y/(N)", choice);
		final String value = ctx.nextLine();
		if ("y".equalsIgnoreCase(value)) {
			activeSection = yesSection;
		} else {
			activeSection = noSection;
		}

		activeSection.requestInput(ctx);
	}

	@Override
	public void writeTo(final Writer writer, final Indent indent, final MavenContext ctx) throws IOException {
		activeSection.writeTo(writer, indent, ctx);
	}

}
