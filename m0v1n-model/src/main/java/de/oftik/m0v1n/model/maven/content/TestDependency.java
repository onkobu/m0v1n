package de.oftik.m0v1n.model.maven.content;

import de.oftik.m0v1n.model.maven.content.DependenciesSection.Scope;

/**
 * Test dependencies.
 *
 * @author onkobu
 *
 */
public class TestDependency extends Dependency {

	public TestDependency(final String groupId, final String artifactId, final String name,
			final String versionProperty, final Version version) {
		super(groupId, artifactId, name, versionProperty, version, Scope.test);
	}

	public TestDependency(final String groupId, final String artifactId, final String name, final Version version) {
		super(groupId, artifactId, name, "version." + artifactId, version, Scope.test);
	}
}
