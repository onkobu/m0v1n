package de.oftik.m0v1n.model.contract;

import de.oftik.m0v1n.model.maven.particles.Comment;

/**
 * Contract for a section that is itself part of a (higher leve) section.
 *
 * @author onkobu
 *
 */
public interface SubSection extends Section {
	@Override
	String getName();

	void setParent(ComplexSection parent);

	String getFullName();

	/**
	 *
	 * @return Descriptive text before the section.
	 */
	Comment getComment();
}
