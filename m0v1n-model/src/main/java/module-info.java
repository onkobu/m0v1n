module m0v1n.model {
	requires java.base;

	requires kehys.lippu;
	requires java.xml;
	requires maven.model;
	requires plexus.utils;

	exports de.oftik.m0v1n.model.contract;
	exports de.oftik.m0v1n.model.content;
	exports de.oftik.m0v1n.model.format;
	exports de.oftik.m0v1n.model.input;
	exports de.oftik.m0v1n.model.maven;
	exports de.oftik.m0v1n.model.maven.content;
	exports de.oftik.m0v1n.model.maven.particles;
}