#!/bin/bash

M0V1N_DIR=$(dirname $0)
VERSION=$(java -version > /dev/null 2>&1 )

err=$?

if [ $err -ne 0 ]; then
	echo "there is no Java Runtime Environment on your machine"
	exit 29
fi

java -cp "$M0V1N_DIR/lib/*" de.oftik.m0v1n.generator.RunQuestionnaire
