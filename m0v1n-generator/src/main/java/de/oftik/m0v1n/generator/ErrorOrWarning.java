package de.oftik.m0v1n.generator;

/**
 * Enumeration of error and warning types.
 */
public enum ErrorOrWarning {
	CANT_WRITE_FILE("File %s is not writable.", 7),

	FILE_ALREADY_EXISTS("File %s already exists and will be overwritten.");

	private final String message;
	private final int exitCode;

	ErrorOrWarning(final String message) {
		this(message, -1);
	}

	ErrorOrWarning(final String message, final int exitCode) {
		this.message = message;
		this.exitCode = exitCode;
	}

	public String getMessage(final Object... values) {
		return String.format(message, values);
	}

	public int exitCode() {
		return exitCode;
	}

}
