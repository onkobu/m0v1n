package de.oftik.m0v1n.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.oftik.m0v1n.model.content.BinaryChoiceSection;
import de.oftik.m0v1n.model.contract.MavenContext;
import de.oftik.m0v1n.model.contract.PomDefinition;
import de.oftik.m0v1n.model.contract.Section;
import de.oftik.m0v1n.model.input.InputValue;
import de.oftik.m0v1n.model.maven.PomXml;
import de.oftik.m0v1n.model.maven.VersionProperties;
import de.oftik.m0v1n.model.maven.content.BuildSection;
import de.oftik.m0v1n.model.maven.content.DependenciesSection;
import de.oftik.m0v1n.model.maven.content.DependencyManagementSection;
import de.oftik.m0v1n.model.maven.content.GeneralSection;
import de.oftik.m0v1n.model.maven.content.InlineTagName;
import de.oftik.m0v1n.model.maven.content.Version;
import de.oftik.m0v1n.model.maven.particles.Comment;
import de.oftik.m0v1n.model.maven.particles.Plugin;

/**
 * Starts asking questions to yield a Maven structure.
 *
 * @author onkobu
 *
 */
public final class RunQuestionnaire {

	enum CliOption {
		HELP("h", "help", false, "print usage and exit"),

		OUTPUT("o", "output", true, "output file");

		private final Option option;

		CliOption(final String shortOpt, final String longOpt, final boolean arg, final String description) {
			this.option = Option.builder(shortOpt).longOpt(longOpt).desc(description).hasArg(arg).build();
		}

		boolean isPresent(final CommandLine cli) {
			return cli.hasOption(option.getOpt());
		}

		String getArgumentValue(final CommandLine cli) {
			return cli.getOptionValue(option.getOpt());
		}

		// If it is not explicitely added, the ClassLoader/ VM will skip this enum's
		// definition as it is not referenced outside
		static Options createOptions() {
			final Options options = new Options();
			for (final CliOption o : values()) {
				options.addOption(o.option);
			}
			return options;
		}
	}

	private static final Options OPTIONS = CliOption.createOptions();

	private RunQuestionnaire() {
	}

	public static void main(final String[] args) throws IOException, ParseException {
		final CommandLineParser p = new DefaultParser();
		final CommandLine cli = p.parse(OPTIONS, args);

		if (CliOption.HELP.isPresent(cli)) {
			printUsageAndExit();
			return;
		}

		final String output = CliOption.OUTPUT.getArgumentValue(cli);

		final Writer writer;
		if (output != null) {
			final File file = new File(String.valueOf(output)).getAbsoluteFile();
			if (file.exists() && (!file.canWrite() || file.isDirectory())) {
				writeError(ErrorOrWarning.CANT_WRITE_FILE, file);
				System.exit(ErrorOrWarning.CANT_WRITE_FILE.exitCode());
			}

			if (file.exists()) {
				writeWarning(ErrorOrWarning.FILE_ALREADY_EXISTS, file);
			} else if (!file.getParentFile().canWrite()) {
				writeError(ErrorOrWarning.CANT_WRITE_FILE, file);
				System.exit(ErrorOrWarning.CANT_WRITE_FILE.exitCode());
			}
			writer = new BufferedWriter(new FileWriter(file));
		} else {
			writer = new OutputStreamWriter(System.out);
		}
		final PomXml questionnaire = new PomXml(
				Arrays.asList(new GeneralSection(), new BinaryChoiceSection("multi module project",
						new DependencyManagementSection(), new DependenciesSection()), new BuildSection()));
		final MavenContext ctx = new PomDefinitionContextImpl(questionnaire);
		questionnaire.start(ctx);
		questionnaire.writeTo(writer, ctx);
		writer.flush();
	}

	private static void writeWarning(final ErrorOrWarning errorOrWarning, final Object... args) {
		System.err.println(errorOrWarning.getMessage(args));
	}

	private static void writeError(final ErrorOrWarning errorOrWarning, final Object... args) {
		System.err.println(errorOrWarning.getMessage(args));
	}

	static class PomDefinitionContextImpl implements MavenContext {
		private final PomDefinition questionnaire;
		private final VersionProperties versionProperties = VersionProperties.instance();
		private Scanner scanner;

		private EnumSet<Flag> flags = EnumSet.noneOf(Flag.class);

		public PomDefinitionContextImpl(final PomDefinition questionnaire) {
			this.questionnaire = questionnaire;
		}

		@Override
		public void setFlag(final Flag flag) {
			flags = EnumSet.of(flag, flags.toArray(new Flag[flags.size()]));
		}

		@Override
		public InputValue findValue(final String vName) {
			for (final Section s : questionnaire.getSections()) {
				final InputValue value = s.findValue(vName);
				if (value != null) {
					return value;
				}
			}
			return null;
		}

		@Override
		public InputValue projectGroupId() {
			return findValue(InlineTagName.groupId.name());
		}

		@Override
		public InputValue projectArtifactId() {
			return findValue(InlineTagName.artifactId.name());
		}

		@Override
		public boolean isSet(final Flag f) {
			return flags.contains(f);
		}

		@Override
		public String getVersionProperty(final String key, final String defaultValue) {
			return versionProperties.getOrDefault(key, defaultValue);
		}

		@Override
		public VersionProperties getVersionProperties() {
			return versionProperties;
		}

		@Override
		public void forcePlugin(final String artifactId, final Version version, final Comment comment) {
			for (final Section s : questionnaire.getSections()) {
				if (s instanceof BuildSection) {
					((BuildSection) s).forcePlugin(new Plugin(artifactId, version, comment));
				}
			}
		}

		@Override
		public void forcePlugin(final Plugin p) {
			for (final Section s : questionnaire.getSections()) {
				if (s instanceof BuildSection) {
					((BuildSection) s).forcePlugin(p);
				}
			}
		}

		@Override
		public String nextLine() {
			if (scanner == null) {
				scanner = new Scanner(System.in);
			}
			return scanner.nextLine();
		}
	}

	private static final void printUsageAndExit() {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("RunPomDefinition", OPTIONS);
	}
}
