# m0v1n

Maven's complexity makes it sometimes hard, to get you m0v1n in the right direction. This command line program asks a handful questions to initialize a meaning- and helpful pom.xml. One can use defaults by providing no input for the questions.

![Sample questionnaire](media/sample-quest.gif)

## General

* dendency versions managed through properties
* multi-module projects (currently init only)

## Dependencies

* Apache Commons CLI
* Hamcrest
* JUnit 4/ 5
* Kotlin, incl. compiler reconfiguration (call Java classes in Kotlin)
* Logging (java.util, Log4J, SLF4J+Log4J, SLF4J+Logback)

## Plugins

* JaCoCo
* PMD
* Maven Enforcer (build reliability)
* Maven Versions (plugin updates)
* OWASP dependency-check

## API

* model POMs through Java classes
* write different styles of projects, e.g. multi module or split repository
* repeatedly set up straightforward projects without Maven archetypes

# TODO

* multi-module add, initial structure with build-tools and app
* Apache checkstyle, with build-tools
* Jackson/Gson
* Mustache
* multi-module with pluginManagement, esp. compiler, surefire, failsafe et al
* create initial structures like src-folder, pay attention to Kotlin
* Jigsaw, Maven forces multi-module projects
* Multi-module with -app and -resources-sub modules
* Spring
